// ==UserScript==
// @name         Proxmox Nag Blocker
// @namespace    https://derenderkeks.me
// @version      0.1.1
// @description  Blocks the subscription nag of Proxmox
// @author       DerEnderkeks
// @match        *://*/*
// @grant        none
// ==/UserScript==

/* eslint no-undef: "off" */
(() => {
    // only run when on a Proxmox page
    if (typeof Proxmox === 'undefined' || Proxmox.Utils?.checked_command == null) {
        return;
    }

    // this completely removes the license check and just directly runs the intended function
    Proxmox.Utils.checked_command = cmd => cmd();
    console.log('Disabled Proxmox Licence Nag!')
})();
