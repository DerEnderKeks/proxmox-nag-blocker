# Proxmox Nag Blocker

This is very simple Userscript that blocks the annoying subscription modal on any Proxmox page. [Click here to install it.](https://gitlab.com/DerEnderKeks/proxmox-nag-blocker/-/raw/main/proxmox-nag-blocker.user.js)
